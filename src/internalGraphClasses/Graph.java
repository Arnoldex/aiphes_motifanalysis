package internalGraphClasses;

import java.util.ArrayList;

/**
 * Created by arnold on 20.04.2015.
 */

// Internal Graph class, contains list of nodes and edges
public class Graph {

    private ArrayList<Node> myNodes = new ArrayList<>();
    private ArrayList<Edge> myEdges = new ArrayList<>();

    public Node nextNode() {
        return new Node(myNodes.size());
    }

    public ArrayList<Node> getMyNodes() {
        return myNodes;
    }

    public void addNode(Node n) {
        myNodes.add(n);
    }

    public ArrayList<Edge> getMyEdges() {
        return myEdges;
    }

    public void addEdge(Edge e) {
        myEdges.add(e);

        e.getN1().addOutgoingEdge(e);
        e.getN2().addIncomingEdge(e);

        if (e.isBidirected()) {

            e.getN1().addIncomingEdge(e);
            e.getN2().addOutgoingEdge(e);

        }
    }
}
