package internalGraphClasses;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by arnold on 20.04.2015.
 */

// Internal Node class that contains String data
public class Node {

    private Queue<Edge> outgoingEdges = new LinkedList<>();
    private Queue<Edge> incomingEdges = new LinkedList<>();

    private String myData = null;

    public String getMyData() {
        return myData;
    }

    public void setMyData(String myData) {
        this.myData = myData;
    }

    public int getMyNumber() {
        return myNumber;
    }

    private int myNumber;

    public Node (int myNumber) {
        this.myNumber = myNumber;
    }

    public Queue<Edge> getOutgoingEdges() {
        return outgoingEdges;
    }

    public Queue<Edge> getIncomingEdges() {
        return incomingEdges;
    }

    public void addOutgoingEdge(Edge e) {
        outgoingEdges.add(e);
    }

    public void addIncomingEdge(Edge e) {
        incomingEdges.add(e);
    }

    public boolean isConnectedToNode(Node n) {

        for (Edge e : outgoingEdges) {
            if (e.getOtherNode(this) == n) return true;
        }
        return false;
    }

}
