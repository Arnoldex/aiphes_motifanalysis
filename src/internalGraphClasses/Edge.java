package internalGraphClasses;

/**
 * Created by arnold on 20.04.2015.
 */

// Internal Edge class, contains no data or labels (yet)
public class Edge {

    private Node n1, n2;
    private boolean bidirected;
    private boolean active;

    public Edge(Node n1, Node n2, boolean bidirected) {
        this.n1 = n1;
        this.n2 = n2;
        this.bidirected = bidirected;
        active = true;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBidirected() {
        return bidirected;
    }

    public Node getOtherNode(Node n) {
        if (n1 == n) return n2;
        return n1;
    }

    public Node getN1() {
        return n1;
    }

    public Node getN2() {
        return n2;
    }
}
