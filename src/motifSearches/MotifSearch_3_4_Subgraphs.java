/**
 * 
 */
package motifSearches;

import internalDataClasses.MotifSignature;
import internalDataClasses.TextObject;
import internalGraphClasses.Edge;
import internalGraphClasses.Graph;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Search for directly adjacent 3 and 4 node connected subgraphs
public class MotifSearch_3_4_Subgraphs extends AbstractMotifSearch {

	private int maximumDistance = 1;
	
	public MotifSignature searchMotifs(TextObject to, Graph graph, String classAttribute) {
		MotifSignature mySignature = new MotifSignature(classAttribute);
		
        for (int i = 0; i < 8; i++) {
        	mySignature.addMotif("M3_" + makeNumber(i, 2), 0);
        }
        
        for (int i = 0; i < 64; i++) {
        	mySignature.addMotif("M4_" + makeNumber(i, 2), 0);
        }

        int anzahlNodes = graph.getMyNodes().size();

        byte[][] adjacencyMatrix = new byte[anzahlNodes][anzahlNodes];

        for (Edge e: graph.getMyEdges()) {
            adjacencyMatrix[e.getN1().getMyNumber()][e.getN2().getMyNumber()] = 1;
            adjacencyMatrix[e.getN2().getMyNumber()][e.getN1().getMyNumber()] = 1;
        }

        for (int a = 0; a < anzahlNodes - 2; a++) {
            for (int b = a + 1; b < Math.min(a + 1 + maximumDistance, anzahlNodes - 1); b++) {
                for (int c = b + 1; c < Math.min(b + 1 + maximumDistance, anzahlNodes); c++) {
                    increaseMotiveCount(adjacencyMatrix, a, b, c, 0, 3, mySignature);
                    for (int d = c + 1; d < Math.min(c + 1 + maximumDistance, anzahlNodes); d++) {
                        increaseMotiveCount(adjacencyMatrix, a, b, c, d, 4, mySignature);
                    }
                }
            }
        }

        mySignature.addMotif("WordCount", to.getWordCount());
        mySignature.addMotif("Textlength", to.getText().length());
        
        clearDisconnectedMotifs(mySignature);
        
        mySignature.normalizeWithPrefix("M3");
        mySignature.normalizeWithPrefix("M4");
		
		return mySignature;
	}
	
	private String makeNumber(int number, int size) {
		
		String ret = String.valueOf(number);
		
		while (ret.length() < size) {
			ret = "0" + ret;
		}
		
		return ret;
		
	}
	
	private void clearDisconnectedMotifs(MotifSignature mySignature) {
		
		mySignature.removeMotif("M3_00");
		mySignature.removeMotif("M3_01");
		mySignature.removeMotif("M3_02");
		mySignature.removeMotif("M3_04");
		
		mySignature.removeMotif("M4_00");
		mySignature.removeMotif("M4_01");
		mySignature.removeMotif("M4_02");
		mySignature.removeMotif("M4_03");
		mySignature.removeMotif("M4_04");
		mySignature.removeMotif("M4_05");
		mySignature.removeMotif("M4_06");
		mySignature.removeMotif("M4_08");
		mySignature.removeMotif("M4_09");
		mySignature.removeMotif("M4_10");
		mySignature.removeMotif("M4_11");
		mySignature.removeMotif("M4_12");
		mySignature.removeMotif("M4_16");
		mySignature.removeMotif("M4_17");
		mySignature.removeMotif("M4_18");
		mySignature.removeMotif("M4_20");
		mySignature.removeMotif("M4_21");
		mySignature.removeMotif("M4_24");
		mySignature.removeMotif("M4_32");
		mySignature.removeMotif("M4_33");
		mySignature.removeMotif("M4_34");
		mySignature.removeMotif("M4_36");
		mySignature.removeMotif("M4_38");
		mySignature.removeMotif("M4_40");
		mySignature.removeMotif("M4_48");
		mySignature.removeMotif("M4_56");
		
	}
	
	private void increaseMotiveCount(byte[][] adjacencyMatrix, int a, int b, int c, int d, int size, MotifSignature sig) {

        int motifNumber;

        if (size == 3) {
            motifNumber = adjacencyMatrix[a][b] * 1 + adjacencyMatrix[a][c] * 2 + adjacencyMatrix[b][c] * 4;
            sig.addMotif("M3_" + makeNumber(motifNumber, 2), 1);
        } else if (size == 4) {
            motifNumber = adjacencyMatrix[a][b] * 1 + adjacencyMatrix[a][c] * 2 + adjacencyMatrix[a][d] * 4 +
                    adjacencyMatrix[b][c] * 8 + adjacencyMatrix[b][d] * 16 + adjacencyMatrix[c][d] * 32;
            sig.addMotif("M4_" + makeNumber(motifNumber, 2), 1);
        }

    }
	
}
