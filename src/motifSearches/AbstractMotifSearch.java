/**
 * 
 */
package motifSearches;

import internalGraphClasses.Graph;
import internalDataClasses.MotifSignature;
import internalDataClasses.TextObject;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Search for Graph motifs and store results in MotifSignature
// Input: Graph and corresponding TextObject
// Output: MotifSignature
public abstract class AbstractMotifSearch {
	
	public abstract MotifSignature searchMotifs(TextObject to, Graph graph, String classAttribute);

}
