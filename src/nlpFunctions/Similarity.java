package nlpFunctions;

import java.util.Arrays;

/**
 * Created by arnold on 13.05.2015.
 */

// Measures for text similarity
public class Similarity {

    // Faster then second approach
    public static float compareSentences(String[] w0, String[] w1) {
        int m = 0;
        //float f = 0;
        // Create a array of words (lowercase) and sort them
        // in alphabetical order

        for (int i = 0; i < w0.length; i++)
            w0[i] = w0[i].toLowerCase();
        Arrays.sort(w0);
        // Same for the second sentence
        for (int i = 0; i < w1.length; i++)
            w1[i] = w1[i].toLowerCase();
        Arrays.sort(w1);
        int p0 = 0, p1 = 0;
        do {
            int comp = w0[p0].compareTo(w1[p1]);
            // println(w0[p0] + "  " + w1[p1] + "  " + comp);
            if (comp == 0) { // same word
                m++;
                p0++;
                p1++;
            }
            else if (comp < 0) { // w0 word is before w1 word
                p0++;
            }
            else if (comp > 0) { // w0 word is after w1 word
                p1++;
            }
        }
        while (p0 < w0.length && p1 < w1.length);
        return (2.0f * m) / ( w0.length + w1.length );
        //System.out.println("Found " + m + " matching words");
        //System.out.println("Simularity factor: " + f);
    }

//    public static void compareSentences2(String x, String y) {
//        String[] xwords = x.toLowerCase().split("[^a-z]+");
//        String[] ywords = y.toLowerCase().split("[^a-z]+");
//        Set<String> xset = new HashSet<String>(Arrays.asList(xwords));
//        Set<String> yset = new HashSet<String>(Arrays.asList(ywords));
//        xset.retainAll(yset);
//
//        //float f = (2.0f * xset.size()) / ( xwords.length + ywords.length );
//
//        //System.out.println("Found " + xset.size() + " matching words");
//        //System.out.println("Simularity factor: " + f);
//
//    }

}
