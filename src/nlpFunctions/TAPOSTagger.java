package nlpFunctions;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTagger;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by arnold on 12.04.2016.
 */

// Sentence segmenter, tokenizer and POS tagger
public class TAPOSTagger {

    public static POSTagger _posTagger = null;
    public static Tokenizer _tokenizer = null;
    public static SentenceDetector _sentenceDect = null;
    private static String modelDirectory = "D:/POSTagModels/";

    public static void initModels(String modelDirectory, String prefix) {
    	TAPOSTagger.modelDirectory = modelDirectory;
    	createTagger(prefix);
    	createTokenizer(prefix);
    	createSentenceDetector(prefix);
    }
    
    public static String[] tag(ArrayList<String> words) {

        String[] myArray = new String[words.size()];
        myArray = words.toArray(myArray);
        return (tag(myArray));

    }

    public static String[] tag(String[] words) {

        if (_posTagger == null) createTagger("de");

        return _posTagger.tag(words);

    }

    public static String[] tokenize(String sentence) {

        if (_tokenizer == null) createTokenizer("de");

        return _tokenizer.tokenize(sentence);

    }

    public static String[] sentenceDetect(String text) {

        if (_sentenceDect == null) createSentenceDetector("de");

        return _sentenceDect.sentDetect(text);

    }

    private static void createTagger(String prefix) {

        InputStream modelIn = null;
        try {
            // Loading tokenizer model
            modelIn = new FileInputStream(modelDirectory + prefix + "-pos-maxent.bin");
            final POSModel posModel = new POSModel(modelIn);
            modelIn.close();

            _posTagger = new POSTaggerME(posModel);
            System.out.println(prefix + " POS Tagger created");

        } catch (final IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (final IOException e) {} // oh well!
            }
        }

    }

    private static void createTokenizer(String prefix) {

        InputStream modelIn = null;
        try {
            // Loading tokenizer model
            modelIn = new FileInputStream(modelDirectory + prefix + "-token.bin");
            final TokenizerModel posModel = new TokenizerModel(modelIn);
            modelIn.close();

            _tokenizer = new TokenizerME(posModel);
            System.out.println(prefix + " Tokenizer created");

        } catch (final IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (final IOException e) {} // oh well!
            }
        }

    }

    private static void createSentenceDetector(String prefix) {

        InputStream modelIn = null;
        try {
            // Loading tokenizer model
            modelIn = new FileInputStream(modelDirectory + prefix + "-sent.bin");
            final SentenceModel posModel = new SentenceModel(modelIn);
            modelIn.close();

            _sentenceDect = new SentenceDetectorME(posModel);
            System.out.println(prefix + " Sentence Detector created");

        } catch (final IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (final IOException e) {} // oh well!
            }
        }

    }

    public static void main(String[] args) {

        ArrayList<String> entityWords = new ArrayList<String>();

        entityWords.add("This");
        entityWords.add("is");
        entityWords.add("my");
        entityWords.add("boomstick");
        entityWords.add(".");

        String[] myArray = new String[entityWords.size()];
        myArray = entityWords.toArray(myArray);

        for (String a : myArray) {
            System.out.println(a);
        }

        String[] outArray = tag(myArray);

        for (String a : outArray) {
            System.out.println(a);
        }

    }

}
