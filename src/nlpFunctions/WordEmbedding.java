/**
 * 
 */
package nlpFunctions;

import internalDataClasses.TextObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import parsers.ParserPlainText;

/**
 * A dataset of word embeddings. Use the cosineSimilarity-function to check for lexical coherence of two words.
 * @author Dominik Pfau
 * 
 */
public class WordEmbedding {
	
	HashMap<String, Double[]> data;

	/**
	 * Construct WordEmbedding from a text file
	 * @param path of the text file
	 */
	public WordEmbedding(String filePath) {
		
		// read raw data from text file
		ParserPlainText myParser = new ParserPlainText();
		TextObject to = myParser.parseFile(filePath);
		
		// create word embeddings representation from String
		readData(to.getText());
	}

	/**
	 * Creates the word embedding data by reading in raw data from a string
	 * @param text String
	 */
	private void readData(String text) {
		
		data = new HashMap<String, Double[]>();
		Scanner sc = new Scanner(text);
		sc.useLocale(Locale.ENGLISH);
		//sc.useDelimiter("\\s|(?<=\\d)(?!\\d)(?![.])");
		
		// proces line after line
		while(sc.hasNextLine()){
			
			Scanner scLine = new Scanner(sc.nextLine());
			LinkedList<Double> myList = new LinkedList<Double>();
		
			// get keyword
			String keyWord = (String) scLine.next();
		
			// get double Vector
			while(scLine.hasNext()){
				myList.add(Double.parseDouble(scLine.next()));
			}
			data.put(keyWord, myList.toArray(new Double[myList.size()]));
			scLine.close();
		}
		sc.close();
	}

	/**
	 * returns true if this object contains a word embedding for the given keyword
	 * @param keyWord
	 * @return true or false
	 */
	public boolean contains(String keyWord){
		return data.containsKey(keyWord);
	}
	
	/**
	 * returns the word embedding vector (Double[]) to the given keyword. If the keyword is not stored, it returns null
	 * @param keyWord
	 * @return
	 */
	public Double[] get(String keyWord){
		return data.get(keyWord);
	}
	
	/**
	 * Returns the cosine similarity value [-1, 1] between two keywords stored in this word embedding representation. 
	 * If one of the given words is NOT stored, the default value of -1 is returned
	 * @param wordA
	 * @param wordB
	 * @return a double between [-1, 1]
	 */
	public double cosineSimilarity(String wordA, String wordB){
		
		if(!contains(wordA) || !contains(wordB)){
			return -1;
		}
		
		Double[] vectorA = data.get(wordA);
		Double[] vectorB = data.get(wordB);
		double dotProduct = 0.0;
	    double normA = 0.0;
	    double normB = 0.0;
	    for (int i = 0; i < vectorA.length; i++) {
	        dotProduct += vectorA[i] * vectorB[i];
	        normA += Math.pow(vectorA[i], 2);
	        normB += Math.pow(vectorB[i], 2);
	    }   
	    return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
	
	/**
	 * for test/debug purposes only! prints out all the word embeddings in String-Double[] pairs to the console.
	 * 
	 */
	public void print(){
		System.out.println("Number of contained words: " + data.size());
		Iterator<Entry<String, Double[]>> it = data.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        System.out.println(pair.getKey() + " = " + Arrays.toString((Double[])pair.getValue()));
	    }
	}
	
	
	/*public static void main(String[] args){
		System.out.println("Starting test");
		System.out.println("reading text file ...");
		WordEmbedding we = new WordEmbedding("E:/glove.6B/glove.6B.50d.txt");
		System.out.println("printing textfile:\n");
		we.print();
		System.out.println("test completed");
	}*/
	
}
