/**
 * 
 */
package main;

import graphConverters.GraphConverterSentenceNouns;
import internalDataClasses.MotifSignature;

import java.io.File;
import java.util.ArrayList;

import outputWriters.ARFFOutputWriter;
import outputWriters.MotifOutputWriter;
import nlpFunctions.TAPOSTagger;
import motifSearches.MotifSearch_3_4_Subgraphs;
import parsers.ParserPlainText;
import threadtask.Parameters;
import threadtask.ThreadTask;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Main class for pipeline
public class MainTest {

	public static void main(String[] args) {
		
		ArrayList<MotifSignature> mySigs = new ArrayList<MotifSignature>();
		
		// Initialize Sentence segmenter, tokenizer and POSTagger with correct language
		// Provide model files in given directory
		// Available languages: German "de", English "en"
		//TAPOSTagger.initModels("D:/POSTagModels/", "en");
		TAPOSTagger.initModels("Ressources/NLPModels/", "en");
		
		// Initialize parameters for pipeline
		// Type of Parser
		// Type of Graph representation
		// Type of MotifSerach algorithm
		Parameters myParameters = new Parameters(new ParserPlainText(), new GraphConverterSentenceNouns(), new MotifSearch_3_4_Subgraphs());
		
		// Add Directory with class labels - TODO: Add multi thread support
		addDirectory(mySigs, "D:/Test/Featured", "Featured", myParameters);
		addDirectory(mySigs, "D:/Test/NonFeatured", "NonFeatured", myParameters);
		
		System.out.println("Writing output files");
		
		// MotifSignatures are ready - write Output files
		MotifOutputWriter.writeMotivCounts(mySigs, "D:/Test/Motives.txt");
		ARFFOutputWriter.writeARFFFiles(mySigs, "D:/Test/WikiMotifs");
		
		System.out.println("Process finished");
		
	}
	
	private static void addDirectory(ArrayList<MotifSignature> mySigs, String dirPath, String classAttribute, Parameters myParameters) {
		
		System.out.println("Adding directory " + dirPath);
		
		File dir = new File(dirPath);
		String[] files = dir.list();
		
		for (String s : files) {
			mySigs.add(new ThreadTask().processFile(dirPath + "/" + s, classAttribute, myParameters));
		}
		
	}
	
}
