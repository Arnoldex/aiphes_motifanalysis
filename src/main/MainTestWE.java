/**
 * 
 */
package main;

import graphConverters.GraphConverterWordEmbeddings;
import internalDataClasses.MotifSignature;

import java.io.File;
import java.util.ArrayList;

import motifSearches.MotifSearch_3_4_Subgraphs;
import nlpFunctions.TAPOSTagger;
import nlpFunctions.WordEmbedding;
import outputWriters.ARFFOutputWriter;
import outputWriters.MotifOutputWriter;
import parsers.ParserPlainText;
import threadtask.Parameters;
import threadtask.ThreadTask;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

/**
 * Main Test-Class for the Word Embeddings
 * @author Dominik
 *
 */
public class MainTestWE {

	public static void main(String[] args) {
		
		ArrayList<MotifSignature> mySigs = new ArrayList<MotifSignature>();
		
		// Initialize Sentence segmenter, tokenizer and POSTagger with correct language
		// Provide model files in given directory
		// Available languages: German "de", English "en"
		//TAPOSTagger.initModels("D:/POSTagModels/", "en");
		TAPOSTagger.initModels("Ressources/NLPModels/", "en");
		
		// Create Word Embeddings from input text file
		// Takes some time, so comment this out if not needed!
		System.out.println("Creating Word Embeddings from file ...");
		WordEmbedding we = new WordEmbedding("E:/glove.6B/glove.6B.50d.txt");
		
		// Initialize parameters for pipeline
		// Type of Parser
		// Type of Graph representation
		// Type of MotifSerach algorithm
		Parameters myParameters = new Parameters(new ParserPlainText(), new GraphConverterWordEmbeddings(we), new MotifSearch_3_4_Subgraphs());

		
		// Add Directory with class labels - TODO: Add multi thread support
		addDirectory(mySigs, "E:/nlp_prak/text-package/summaryL1/summaryL1", "L1", myParameters);
		addDirectory(mySigs, "E:/nlp_prak/text-package/summaryL2", "L2", myParameters);
		
		System.out.println("Writing output files");
		
		// MotifSignatures are ready - write Output files
		MotifOutputWriter.writeMotivCounts(mySigs, "E:/nlp_prak/summary_testrun/Motives.txt");
		ARFFOutputWriter.writeARFFFiles(mySigs, "E:/nlp_prak/summary_testrun/summaryMotifs");
		
		System.out.println("Process finished");
		
	}
	
	private static void addDirectory(ArrayList<MotifSignature> mySigs, String dirPath, String classAttribute, Parameters myParameters) {
		
		System.out.println("Adding directory " + dirPath);
		
		File dir = new File(dirPath);
		String[] files = dir.list();
		
		for (String s : files) {
			mySigs.add(new ThreadTask().processFile(dirPath + "/" + s, classAttribute, myParameters));
		}
		
	}
	
}
