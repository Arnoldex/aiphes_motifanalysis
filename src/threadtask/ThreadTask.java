/**
 * 
 */
package threadtask;

import internalGraphClasses.Graph;
import internalDataClasses.MotifSignature;
import internalDataClasses.TextObject;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Pipeline for one single file
// Input: Path to file
// Processing steps: Parser, GraphConverter, MotifSearch
// Output: MotifSignature
public class ThreadTask {
	
	private TextObject to;
	private Graph graph;
	private MotifSignature signature;
	
	public MotifSignature processFile(String filePath, String classAttribute, Parameters myParameters) {
		
		to = myParameters.getMyParser().parseFile(filePath);
		graph = myParameters.getMyGraphConverter().convertToGraph(to);
		signature = myParameters.getMyMotifSearch().searchMotifs(to, graph, classAttribute);
		
		return signature;
		
	}
	
	/**
	 * @return the graph
	 */
	public Graph getGraph() {
		return graph;
	}
	
	/**
	 * @return the signature
	 */
	public MotifSignature getSignature() {
		return signature;
	}
	
	/**
	 * @return the to
	 */
	public TextObject getTextObject() {
		return to;
	}
	
}
