/**
 * 
 */
package threadtask;

import motifSearches.AbstractMotifSearch;
import graphConverters.AbstractGraphConverter;
import parsers.AbstractParser;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Parameters for Graph analysis pipeline:
// Type of Parser
// Type of Graph representation
// Type of MotifSerach algorithm
public class Parameters {

	private AbstractParser myParser;
	private AbstractGraphConverter myGraphConverter;
	private AbstractMotifSearch myMotifSearch;
	
	public Parameters(AbstractParser myParser, AbstractGraphConverter myGraphConverter, AbstractMotifSearch myMotifSearch) {
		this.myParser = myParser;
		this.myGraphConverter = myGraphConverter;
		this.myMotifSearch = myMotifSearch;
	}
	
	/**
	 * @return the myGraphConverter
	 */
	public AbstractGraphConverter getMyGraphConverter() {
		return myGraphConverter;
	}
	
	/**
	 * @return the myMotifSearch
	 */
	public AbstractMotifSearch getMyMotifSearch() {
		return myMotifSearch;
	}
	
	/**
	 * @return the myParser
	 */
	public AbstractParser getMyParser() {
		return myParser;
	}
	
}
