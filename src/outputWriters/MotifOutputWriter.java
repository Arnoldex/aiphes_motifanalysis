/**
 * 
 */
package outputWriters;

import internalDataClasses.MotifSignature;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Write raw Motif counts to plain text file 
public class MotifOutputWriter {

	public static void writeMotivCounts(ArrayList<MotifSignature> sigs, String outputFile) {
		
		MotifSignature currentSig;
		String[] attributeNames;
		
		try {
			
			if (sigs.size() > 0) {
				
				FileWriter writer = new FileWriter(outputFile);
				
				currentSig = sigs.get(0);
				attributeNames = currentSig.getMotifMap().keySet().toArray(new String[currentSig.getMotifMap().keySet().size()]);
				Arrays.sort(attributeNames);
				
				for (String s : attributeNames) {
					writer.write(s);
					writer.write(";");
				}
				writer.write("class");
				
				writer.write(System.lineSeparator());
				
				for (MotifSignature sig : sigs) {
					
					for (String s : attributeNames) {
						writer.write(String.valueOf(sig.getMotifMap().get(s)));
						writer.write(";");
					}
					writer.write(sig.getClassAttribute());
					writer.write(System.lineSeparator());
					
				}
				
				writer.close();
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
