/**
 * 
 */
package outputWriters;

import internalDataClasses.MotifSignature;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Writes ARFF files for every category combination (attribute category defined by first two letters)
// ARFF files are used in Weka machine learning tools
public class ARFFOutputWriter {

	public static void writeARFFFiles(ArrayList<MotifSignature> sigs, String outputFile) {
		
		MotifSignature currentSig;
		String[] attributeNames;
		HashSet<String> classNameSet = new HashSet<String>();
		String[] classNames;
		
		try {
			
			if (sigs.size() > 0) {
				
				FileWriter writer;
				
				currentSig = sigs.get(0);
				attributeNames = currentSig.getMotifMap().keySet().toArray(new String[currentSig.getMotifMap().keySet().size()]);
				Arrays.sort(attributeNames);
				
				for (MotifSignature sig : sigs) {
					classNameSet.add(sig.getClassAttribute());
				}
				
				classNames = classNameSet.toArray(new String[classNameSet.size()]);
				Arrays.sort(classNames);
				
				HashMap<String, Boolean> categories = new HashMap<String, Boolean>();
				
				for (String s : attributeNames) {
					if (!categories.containsKey(s.substring(0, 2))) {
						categories.put(s.substring(0, 2), false);
					}
				}
				
				String[] categoryArray = categories.keySet().toArray(new String[categories.size()]);
				int numberOfCategories = categoryArray.length;
				int numberOfFiles = (int)Math.pow(2, numberOfCategories);
				String fileSuffix;
				boolean tempUse;
				
				for (int i = 0; i < numberOfFiles - 1; i++) {
					
					fileSuffix = "";
					
					for (int cat = 0; cat < numberOfCategories; cat++) {
						tempUse = ( (i / (int)Math.pow(2, cat)) % 2 == 0);
						categories.put(categoryArray[cat], tempUse);
						if (tempUse) {
							fileSuffix += categoryArray[cat];
						}
					}
					
					writer = new FileWriter(outputFile + "_" + fileSuffix + ".arff");
					
					writer.write("@RELATION " + outputFile + "_" + fileSuffix);
					writer.write(System.lineSeparator());
					writer.write(System.lineSeparator());
					
					for (String att : attributeNames) {
						
						if (categories.get(att.substring(0, 2))) {
							writer.write("@ATTRIBUTE " + att + " NUMERIC");
							writer.write(System.lineSeparator());
						}
						
					}
					
					writer.write("@ATTRIBUTE class {");
					
					for (int cn = 0; cn < classNames.length; cn++) {
						if (cn != 0) writer.write(","); 
						writer.write(classNames[cn]);
					}
					
					writer.write("}");
					writer.write(System.lineSeparator());	
					writer.write(System.lineSeparator());
					
					writer.write("@DATA");
					writer.write(System.lineSeparator());
					
					for (MotifSignature sig : sigs) {
						for (String s : attributeNames) {
							if (categories.get(s.substring(0, 2))) {
								writer.write(String.valueOf(sig.getMotifMapNormalized().get(s)));
								writer.write(",");
							}
						}
						writer.write(sig.getClassAttribute());
						writer.write(System.lineSeparator());
					}
					
					writer.close();
					
				}
				
				
				
//				writer.write("class");
//				
//				writer.write(System.lineSeparator());
//				
//				for (MotifSignature sig : sigs) {
//					
//					for (String s : attributeNames) {
//						writer.write(String.valueOf(sig.getMotifMap().get(s)));
//						writer.write(";");
//					}
//					writer.write(sig.getClassAttribute());
//					writer.write(System.lineSeparator());
//					
//				}
//				
//				writer.close();
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
