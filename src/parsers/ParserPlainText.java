/**
 * 
 */
package parsers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import internalDataClasses.TextObject;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Simple plain text parser for unformatted txt files
public class ParserPlainText extends AbstractParser {

	public TextObject parseFile(String filePath) {
		
		try {
			File file = new File(filePath);
			String content = readFile(filePath, StandardCharsets.UTF_8);
			return new TextObject(file.getName(), content);			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	};
	
	static String readFile(String path, Charset encoding) throws IOException{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
}
