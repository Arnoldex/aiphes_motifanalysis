/**
 * 
 */
package parsers;

import internalDataClasses.TextObject;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// File parsers
// Input: Path to file
// Output: Textobject that contains name and text
public abstract class AbstractParser {

	public abstract TextObject parseFile(String filePath);
	
}
