/**
 * 
 */
package graphConverters;

import internalDataClasses.TextObject;
import internalGraphClasses.Edge;
import internalGraphClasses.Graph;

import java.util.ArrayList;
import java.util.Arrays;

import nlpFunctions.TAPOSTagger;
import nlpFunctions.WordEmbedding;

/**
 * Every sentence is a node
* Two nodes are connected with an edge if and only if: 
* - They contain a pair of nouns of exact match or with cosine similarity above a threshold
* - They are at most X sentences apart from each other
 * @author Dominik Pfau
 */

public class GraphConverterWordEmbeddings extends AbstractGraphConverter {
	
	private WordEmbedding we;
	private int sentenceDistanceMax = 3;

	/**
	 * 
	 */
	public GraphConverterWordEmbeddings(WordEmbedding wordEmbedding) {
		this.we = wordEmbedding;
	}

	/* (non-Javadoc)
	 * @see graphConverters.AbstractGraphConverter#convertToGraph(internalDataClasses.TextObject)
	 */
	@Override
	public Graph convertToGraph(TextObject to) {
		
		String text = to.getText();
		
		// Convert text to sentences
        double threshold = 0.0;

        String[] sentences = TAPOSTagger.sentenceDetect(text);
        String[] words;

        ArrayList<String> entityWords = new ArrayList<>();
        ArrayList<String[]> entitySentences = new ArrayList<>();

        Graph myGraph = new Graph();
        int wordCount = 0;
        String[] posTags;

        for (int i = 0; i < sentences.length; i++) {

            entityWords.clear();
            words = TAPOSTagger.tokenize(sentences[i]);
            posTags = TAPOSTagger.tag(words);

            wordCount += words.length;
            if (words.length >= 3) {

                for (int j = 0; j < words.length; j++) {
                    if (posTags[j].startsWith("N")) {
                        entityWords.add(words[j]);
                    }
                }

                if (entityWords.size() > 0) {
                    entitySentences.add(entityWords.toArray(new String[entityWords.size()]));
                    
                    //create Graph nodes
                    myGraph.addNode(myGraph.nextNode());
                    myGraph.getMyNodes().get(myGraph.getMyNodes().size() - 1).setMyData(sentences[i]);
                }
            }
        }
        
        // Connect Graph
        for (int i = 0; i < entitySentences.size(); i++) {
            for (int j = i - 1; j >= Math.max(0, i-sentenceDistanceMax); j--) { // Sentence distance max! Important variable!
                // Compare with all existing sentences up to distance sentenceDistanceMax
                if (getHighestCosSim(entitySentences.get(i), entitySentences.get(j)) >= threshold) {
                    myGraph.addEdge(new Edge(myGraph.getMyNodes().get(i), myGraph.getMyNodes().get(j), true));
                }
            }
        }

        to.setWordCount(wordCount);

        return myGraph;

	}

	/**
	 * Returns the highest cosine similarity value of every possible pair of words between the two given sentences. 
	 * Thus giving a measurement of the semantic coherence of these two sentences.
	 * @param strings
	 * @param strings2
	 * @return double value between [-1, 1]
	 */
	private double getHighestCosSim(String[] s1, String[] s2) {
		
		// default value = worst possible Value
		double simVal = -1.0;
		
        // Create a array of words (lowercase) and sort them
        // in alphabetical order
		for (int i = 0; i < s1.length; i++)
            s1[i] = s1[i].toLowerCase();
        Arrays.sort(s1);
        // Same for the second sentence
        for (int i = 0; i < s2.length; i++)
            s2[i] = s2[i].toLowerCase();
        Arrays.sort(s2);
        
        // check every word from s1 against every word of s2
        for(int i = 0; i < s1.length; i++){
        	for(int j = 0; j < s2.length; j++){
        		simVal = Math.max(simVal, we.cosineSimilarity(s1[i], s2[j]));
        	}
        }
        
        return simVal;
	}

}
