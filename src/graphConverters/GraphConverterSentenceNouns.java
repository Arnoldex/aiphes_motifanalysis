/**
 * 
 */
package graphConverters;

import internalDataClasses.TextObject;
import internalGraphClasses.Edge;
import internalGraphClasses.Graph;

import java.util.ArrayList;

import nlpFunctions.Similarity;
import nlpFunctions.TAPOSTagger;



/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Every sentence is a node
// Two nodes are connected with an edge if and only if:
// - They share a noun by exact match
// - They are at most 3 sentences apart from each other
public class GraphConverterSentenceNouns extends AbstractGraphConverter {
	
	public Graph convertToGraph(TextObject to) {
		
		String text = to.getText();
		
		// Convert text to sentences
        float fThreshold = 0.01f;

        String[] sentences = TAPOSTagger.sentenceDetect(text);
        String[] words;

        ArrayList<String> entityWords = new ArrayList<>();
        ArrayList<String[]> entitySentences = new ArrayList<>();

        Graph myGraph = new Graph();
        int wordCount = 0;
        String[] posTags;

        for (int i = 0; i < sentences.length; i++) {

            entityWords.clear();
            words = TAPOSTagger.tokenize(sentences[i]);
            posTags = TAPOSTagger.tag(words);

            wordCount += words.length;
            if (words.length >= 3) {

                for (int j = 0; j < words.length; j++) {
                    if (posTags[j].startsWith("N")) {
                        entityWords.add(words[j]);
                    }
                }

                if (entityWords.size() > 0) {
                    entitySentences.add(entityWords.toArray(new String[entityWords.size()]));

                    myGraph.addNode(myGraph.nextNode());
                    myGraph.getMyNodes().get(myGraph.getMyNodes().size() - 1).setMyData(sentences[i]);
//                    myGraph.getMyNodes().get(myGraph.getMyNodes().size() - 1).setMyEntities(entitySentences.get(entitySentences.size() - 1));
                }
            }

        }

        int sentenceDistanceMax = 3;

        // Connect Graph
        for (int i = 0; i < entitySentences.size(); i++) {
            for (int j = i - 1; j >= Math.max(0, i-sentenceDistanceMax); j--) { // Sentence distance max! Important variable!
                // Compare with all existing sentences up to distance sentenceDistanceMax
                if (Similarity.compareSentences(entitySentences.get(i), entitySentences.get(j)) >= fThreshold) {
                    myGraph.addEdge(new Edge(myGraph.getMyNodes().get(i), myGraph.getMyNodes().get(j), true));
                }
            }
        }

        to.setWordCount(wordCount);

        return myGraph;
		
	};

}
