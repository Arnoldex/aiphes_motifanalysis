/**
 * 
 */
package graphConverters;

import internalGraphClasses.Graph;
import internalDataClasses.TextObject;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Convert TextObject to Graph object, implements chosen graph representation
// Input: TextObject
// Output: Graph
public abstract class AbstractGraphConverter {
	
	public abstract Graph convertToGraph(TextObject to);

}
