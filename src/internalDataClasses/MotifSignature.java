/**
 * 
 */
package internalDataClasses;

import java.util.HashMap;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Data class that contains motif signature
public class MotifSignature {

	// motifMap contains raw counts
	private HashMap<String, Integer> motifMap = new HashMap<String, Integer>();
	// motifMapNormalized contains Counts normalized to 1 by category
	// Has to called specifically with normalizeWithPrefix()
	private HashMap<String, Double> motifMapNormalized = new HashMap<String, Double>();
	private String classAttribute;
	
	public MotifSignature(String classAttribute) {
		this.classAttribute = classAttribute;
	}
	
	public HashMap<String, Integer> getMotifMap() {
		return motifMap;
	}
	
	public HashMap<String, Double> getMotifMapNormalized() {
		return motifMapNormalized;
	}
	
	public void addMotif(String motif, int count) {
		if (!motifMap.containsKey(motif)) {
			motifMap.put(motif, 0);
			motifMapNormalized.put(motif, 0.0);
		}
		motifMap.put(motif, motifMap.get(motif) + count);
		motifMapNormalized.put(motif, motifMapNormalized.get(motif) + count);
	}
	
	public void removeMotif(String motif) {
		motifMap.remove(motif);
		motifMapNormalized.remove(motif);
	}
	
	public void normalizeWithPrefix(String prefix) {
		
		double sum = 0;
		
		for (String motif : motifMapNormalized.keySet()) {
			
			if (motif.startsWith(prefix)) {
				
				sum += motifMapNormalized.get(motif);
			}
			
		}
		
		for (String motif : motifMapNormalized.keySet()) {
			
			if (motif.startsWith(prefix)) {
				
				motifMapNormalized.put(motif, motifMapNormalized.get(motif) / sum);
				
			}
			
		}
		
	}
	
	/**
	 * @return the classAttribute
	 */
	public String getClassAttribute() {
		return classAttribute;
	}
		
}
