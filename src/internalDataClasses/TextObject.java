/**
 * 
 */
package internalDataClasses;

/**
 * @author Thomas Arnold (arnoldex@web.de)
 * 04.05.2016
 */

// Contains title and text of one document
public class TextObject {

	private String title;
	private String text;
	private int wordCount = 0;
	
	public TextObject(String title, String text) {
		
		this.title = title;
		this.text = text;
		
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * @param wordCount the wordCount to set
	 */
	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}
	
	/**
	 * @return the wordCount
	 */
	public int getWordCount() {
		return wordCount;
	}
	
}
